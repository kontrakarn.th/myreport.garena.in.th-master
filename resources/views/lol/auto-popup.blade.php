@extends('header')

@section('content')
    <style>
        select {
            height: 35px !important;
        }
    </style>
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Auto popup </h5>
                        <hr>
                        <form action="{{route('lol.autoPopup.save')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <p>URL</p>
                                <input type="text" name="url" class="form-control" placeholder="URL" required value="{{$popup->url}}">
                            </div>
                            <button class="btn btn-info" style="width: 200px">Save</button>
                            <a href="{{route('lol.autoPopup.cache')}}">
                                <span class="btn btn-info" style="width: 200px">Clear cache</span>
                            </a>
                        </form>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 mb-2 text-center">
                                <h4>Server <b>Test</b></h4>
                                @if ($popup->status_test === "inactive")
                                    <button class="btn btn-danger btn-lg btn-block" onClick="updateStatus('test')">
                                        Inactive
                                    </button>
                                @else
                                    <button class="btn btn-success btn-lg btn-block" onClick="updateStatus('test')">
                                        Active
                                    </button>
                                @endif
                                <a href="https://lolapi.garena.co.th/auto-popup-test" target="_blank"><small>Link</small></a>
                            </div>
                            <div class="col-sm-12 col-md-6 mb-2 text-center">
                                <h4>Server <b>Live</b></h4>
                                @if ($popup->status_live === "inactive")
                                    <button class="btn btn-danger btn-lg btn-block" onClick="updateStatus('live')">
                                        Inactive
                                    </button>
                                @else
                                    <button class="btn btn-success btn-lg btn-block" onClick="updateStatus('live')">
                                        Active
                                    </button>
                                @endif
                                <a href="https://lolapi.garena.co.th/auto-popup" target="_blank"><small>Link</small></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

        function updateStatus(type) {
            swal({
                title: "Are you sure?",
                text: "Do you want to update status auto popup",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            url: '{{route('lol.autoPopup.status')}}',
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                            },
                            data: JSON.stringify({
                                type: type,
                            }),
                            contentType: "application/json",
                            success: function (data) {
                                if (!data.status) {
                                    swal({
                                        title: "Error",
                                        text: data.message,
                                        icon: "error",
                                    })
                                    return;
                                }

                                window.location.reload();
                            },
                            error: function (resp) {
                                swal({
                                    title: "Error",
                                    text: "Error",
                                    icon: "error",
                                })
                            }
                        });

                    } else {
                        // swal("Canceled");
                    }
                });
        }

    </script>

@endsection
