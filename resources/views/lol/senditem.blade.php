@extends('header')

@section('content')
    <style>
        select {
            height: 35px !important;
        }
    </style>
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Send items logs</h5>
                        <table class="table table-hover">
                            <tr>
                                <td><b>Datetime</b></td>
                                <td><b>UID</b></td>
                                <td><b>Purpose</b></td>
                                <td><b>Amount</b></td>
                                <td><b>Type</b></td>
                                <td><b>Purpose</b></td>
                                <td><b>Status</b></td>
                                <td><b>Send By</b></td>
                            </tr>
                            @foreach($sendlogs as $sendlog)
                                <tr>
                                    <td>{{$sendlog->sended_at}}</td>
                                    <td>{{$sendlog->uid}}</td>
                                    <td>{{$sendlog->key_id}}</td>
                                    <td>{{$sendlog->amount}}</td>
                                    <td>{{$sendlog->inventory_type}}</td>
                                    <td>{{$sendlog->purpose}}</td>
                                    <td>
                                        @if ($sendlog->status == "pending")
                                            <span class="badge badge-info">Pending</span>
                                        @elseif ($sendlog->status == "success")
                                            <span class="badge badge-success">Success</span>
                                        @elseif ($sendlog->status == "failed")
                                            <span class="badge badge-danger">Failed</span>
                                        @elseif ($sendlog->status == "sending")
                                            <span class="badge badge-warning">Sending</span>
                                        @endif
                                    </td>
                                    <td>{{$sendlog->sended_by}}</td>
                                </tr>
                            @endforeach
                        </table>
                        {{$sendlogs->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
