@extends('header')

@section('content')
    <style>
        select {
            height: 35px !important;
        }
    </style>
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Visitors IP Hub</h5>
                        <table class="table table-hover">
                            <tr>
                                <td><b>Datetime</b></td>
                                <td><b>Real IP</b></td>
                                <td><b>X forward IP</b></td>
                                <td><b>User agent</b></td>
                                <td><b>Origin</b></td>
                            </tr>
                            @foreach($visitors as $visitor)
                                <tr>
                                    <td>{{$visitor->created_at}}</td>
                                    <td>{{$visitor->real_ip}}</td>
                                    <td>{{$visitor->x_forwarded_ip}}</td>
                                    <td><small>{{$visitor->user_agent}}</small></td>
                                    <td>{{$visitor->origin}}</td>
                                </tr>
                            @endforeach
                        </table>
                        {{$visitors->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
