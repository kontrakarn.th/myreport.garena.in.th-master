@extends('header')

@section('style')
    <style>

    </style>
@endsection


@section('content')
    <div class="panel-header panel-header-lg">
        <canvas id="bigDashboardChart"></canvas>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="header text-center">
                            <h4 class="title">{{$event->event_name}}</h4>
                            <h6>{{\Carbon\Carbon::parse($event->event_start)->format("d/m/Y")}} - {{\Carbon\Carbon::parse($event->event_end)->format("d/m/Y")}}</h6>
                            <span class="badge badge-success">{{$event->event_slug}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-chart">
                    <div class="card-header">
                        <h4 class="card-title">Send logs</h4>
                    </div>
                    <div class="card-body">
                        <div class="chart-area" style="height: 300px">
                            <canvas id="sendlogsChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            var ctx = document.getElementById('bigDashboardChart').getContext("2d");
            var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, '#80b6f4');
            gradientStroke.addColorStop(1, "#FFFFFF");

            var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

            var x = {!! json_encode($daily["name"]) !!};
            var y = {!! json_encode($daily["count"]) !!};

            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: x,
                    datasets: [{
                        label: "Data",
                        borderColor: "#FFFFFF",
                        pointBorderColor: "#FFFFFF",
                        pointBackgroundColor: "#1e3d60",
                        pointRadius: 5,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: y
                    }]
                },
                options: {
                    layout: {
                        padding: {
                            left: 20,
                            right: 20,
                            top: 0,
                            bottom: 0
                        }
                    },
                    maintainAspectRatio: false,
                    tooltips: {
                        backgroundColor: '#fff',
                        titleFontColor: '#333',
                        bodyFontColor: '#666',
                        bodySpacing: 4,
                        xPadding: 12,
                        mode: "nearest",
                        intersect: 0,
                        position: "nearest"
                    },
                    legend: {
                        position: "bottom",
                        fillStyle: "#FFF",
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                fontColor: "rgba(255,255,255,0.4)",
                                fontStyle: "bold",
                                beginAtZero: true,
                                maxTicksLimit: 5,
                                padding: 10
                            },
                            gridLines: {
                                drawTicks: true,
                                drawBorder: false,
                                display: true,
                                color: "rgba(255,255,255,0.1)",
                                zeroLineColor: "transparent"
                            }

                        }],
                        xAxes: [{
                            gridLines: {
                                zeroLineColor: "transparent",
                                display: false,

                            },
                            ticks: {
                                padding: 10,
                                fontColor: "rgba(255,255,255,0.4)",
                                fontStyle: "bold"
                            }
                        }]
                    }
                }
            });

            var ctx2 = document.getElementById('sendlogsChart').getContext("2d");

            var myBarChart = new Chart(ctx2, {
                type: 'bar',
                height:"10px",
                data: {
                    labels: ["Success", "In queue", "Fail"],
                    datasets: [{
                        label: 'Send logs',
                        data: {!! json_encode($sendlogs) !!},
                        backgroundColor: [
                            'rgba(126,180,0,0.78)',
                            'rgba(246,199,0,0.69)',
                            'rgba(255,18,0,0.75)',
                        ],
                        borderWidth: 2
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                        padding: {
                            left: 10,
                            right: 10,
                            top: 0,
                            bottom: 0
                        },
                    },
                }
            });
        })
    </script>
@endsection
