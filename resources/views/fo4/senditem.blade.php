@extends('header')

@section('content')
    <style>
        select {
            height: 35px !important;
        }
    </style>
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Send items</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('fo4.senditem.post')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Account id <small>( FO4IT02 : 9dc101360b91bbc1b546355e )</small></p>
                                        <input type="text" name="account_id" class="form-control"
                                               placeholder="Account id" required>

                                    </div>
                                </div>
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <p>Product ID</p>
                                        <input type="number" name="product_id" class="form-control"
                                               placeholder="Product ID" required value="{{request()->product_id ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <p>Event</p>
                                        <select name="event" class="form-control" required>
                                            <option value="" disabled selected>Please select event.</option>
                                            @foreach($events as $event)
                                                <option value="{{$event->event_name}}" {{request()->event == $event->event_name ? "selected" : null}}>{{$event->event_name}}</option>
                                            @endforeach
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <p>Purpose</p>
                                        <input type="text" name="purpose" class="form-control" placeholder="Purpose"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-3 pr-1">
                                    <div class="form-group">
                                        <p>Count</p>
                                        <input type="number" name="count" class="form-control" placeholder="Count"
                                               required value="1" max="2" min="1">
                                    </div>
                                </div>
                                <div class="col-md-3 pr-5 mt-4">
                                    <button class="btn btn-success btn-block">Send item</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Send items logs</h5>
                        <table class="table table-hover">
                            <tr>
                                <td><b>Datetime</b></td>
                                <td><b>Account ID</b></td>
                                <td><b>Product ID</b></td>
                                <td><b>Event</b></td>
                                <td><b>Purpose</b></td>
                                <td><b>Count</b></td>
                                <td><b>Sender</b></td>
                                <td><b>Status</b></td>
                            </tr>
                            @foreach($sendlogs as $sendlog)
                                <tr>
                                    <td>{{$sendlog->created_at_str}}</td>
                                    <td>{{$sendlog->account_id}}</td>
                                    <td>{{$sendlog->product_id}}</td>
                                    <td>{{$sendlog->event}}</td>
                                    <td>{{$sendlog->purpose}}</td>
                                    <td>{{$sendlog->count}}</td>
                                    <td>
                                        <small class="">{{$sendlog->send_by}}</small>
                                    </td>
                                    <td>
                                        @if ($sendlog->status == "pending")
                                            <span class="badge badge-info">Pending</span>
                                        @elseif ($sendlog->status == "success")
                                            <span class="badge badge-success">Success</span>
                                        @elseif ($sendlog->status == "fail")
                                            <span class="badge badge-danger">Failed</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{$sendlogs->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
