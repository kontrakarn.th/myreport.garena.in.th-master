@extends('header')

@section('content')
    <style>
        select {
            height: 35px !important;
        }
    </style>
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Edit {{$event->event_name}}</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('fo4.edit.post',[$event])}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Event Name</p>
                                        <input type="text" name="event_name" class="form-control"
                                               placeholder="Event name" required value="{{$event->event_name}}">

                                    </div>
                                </div>
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Slug <small>fo4_<span class="text-danger">xxxxxxxx</span></small></p>
                                        <input type="text" name="event_slug" class="form-control"
                                               placeholder="Event slug" required value="{{$event->event_slug}}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Start Event Date</p>
                                        <input type="text" name="event_start" class="form-control" placeholder="Start Event Date"
                                               required value="{{$event->event_start}}">
                                    </div>
                                </div>

                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>End API Date</p>
                                        <input type="text" name="api_end" class="form-control" placeholder="End API Date"
                                               required value="{{$event->api_end}}">
                                    </div>
                                </div>

                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>End Event Date</p>
                                        <input type="text" name="event_end" class="form-control" placeholder="End Event Date"
                                               required value="{{$event->event_end}}">
                                    </div>
                                </div>

                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Client ID</p>
                                        <input type="text" name="client_id" class="form-control" placeholder="Client ID"
                                               required value="{{$event->client_id}}">
                                    </div>
                                </div>

                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Default Group</p>
                                        <input type="text" name="default_group" class="form-control" placeholder="Link Video"
                                               required value="{{$event->default_group}}">
                                    </div>
                                </div>

                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <p>Link Video</p>
                                        <input type="text" name="link_video" class="form-control" placeholder="Link Video"
                                               required value="{{$event->link_video}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 offset-md-4 pr-5 mt-4">
                                    <button class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script >
        'use strict';
        $(function() {
            console.log('pass')
            $('input[name="event_start"],input[name="event_end"],input[name="api_end"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            });
        });

    </script>
@endsection
