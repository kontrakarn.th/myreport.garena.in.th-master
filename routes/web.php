<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');

Route::group(['prefix' => 'fo4', 'namespace' => 'fo4'], function () {
    Route::get('/', 'IndexController@index')->name('fo4.index');
    Route::get('/change-env', 'IndexController@changeEnv')->name('fo4.change-env');

    Route::get('/add', 'IndexController@add')->name('fo4.add');
    Route::post('/add', 'IndexController@addStore')->name('fo4.add.post');

    Route::get('/senditem', 'IndexController@senditem')->name('fo4.senditem');
    Route::post('/senditem', 'IndexController@senditemPost')->name('fo4.senditem.post');

    Route::post('/event/update-status', 'IndexController@eventUpdateStatus')->name('fo4.event.update-status');

    Route::get('/{event}/dashboard', 'IndexController@dashboard')->name('fo4.dashboard');
    Route::get('/{event}/edit', 'IndexController@eventEdit')->name('fo4.edit');
    Route::post('/{event}/edit', 'IndexController@eventEditPost')->name('fo4.edit.post');
});

Route::group(['prefix' => 'lol', 'namespace' => 'lol'], function () {
    Route::get('/senditem', 'IndexController@senditem')->name('lol.senditem');
    Route::get('/visitor-hub', 'IndexController@visitorHub')->name('lol.visitorHub');
    Route::get('/auto-popup', 'IndexController@autoPopup')->name('lol.autoPopup');
    Route::post('/auto-popup', 'IndexController@autoPopupSave')->name('lol.autoPopup.save');
    Route::post('/auto-popup-status', 'IndexController@autoPopupStatus')->name('lol.autoPopup.status');
    Route::get('/auto-popup-cache', 'IndexController@autoPopupClearCache')->name('lol.autoPopup.cache');
});
