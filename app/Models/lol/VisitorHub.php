<?php

namespace App\Models\lol;

use \Illuminate\Database\Eloquent\Model;

class VisitorHub extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_events_lol';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'all_visitor_hub';

}
