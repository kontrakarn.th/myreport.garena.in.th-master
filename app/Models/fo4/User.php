<?php

namespace App\Models\fo4;

use App\Http\Controllers\BindsDynamically;
use Jenssegers\Mongodb\Eloquent\Model;

class User extends Model
{
    use BindsDynamically;
}
