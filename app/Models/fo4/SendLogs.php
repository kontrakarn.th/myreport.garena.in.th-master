<?php

namespace App\Models\fo4;

use Jenssegers\Mongodb\Eloquent\Model;

class SendLogs extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb_fo4';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'fo4_all_events_send_logs';

}
