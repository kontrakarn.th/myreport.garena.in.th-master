<?php

namespace App\Models\aov;

use Cache;
use Illuminate\Database\Eloquent\Model;
use Storage;

class SendLogs extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'garena_rescuealice_rov_test';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rescuealice_testresend_send_logs';

    protected $fillable = ['status','response'];

}
