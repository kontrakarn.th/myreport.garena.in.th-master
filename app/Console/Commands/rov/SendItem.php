<?php

namespace App\Console\Commands\rov;

use App\Models\aov\SendLogs;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rov:resend_items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $name = $this->ask('What is your name ?');
        if (empty($name) || $name == "" || $name === null) {
            $this->info(" Pls, enter your name!, Goodbye.");
            return;
        }

        $client_id = $this->ask('What is your client_id ?');
        if (empty($client_id) || $client_id == "" || $client_id === null) {
            $this->info(" Pls, enter your client_id!, Goodbye.");
            return;
        }

        //get logs
        $logs = SendLogs::whereStatus('pending')
            ->where('response',null)
            ->get();
        if ($logs->count() <= 0) {
            $this->info(" Not have items for send, Goodbye.");
            return;
        }
        $check = $this->ask('Do you want to sending '.$logs->count().' items ?? (Y/n)');
        if ($check != "Y" && $check != "y") {
            $this->info(" Goodbye.");
            return;
        }

        $this->info(" Prepare to sending...");
        $this->info(" ---------------------------");

        $total   = 0;
        $success = 0;
        foreach ($logs as $log) {
            $tencent_id    = $log->tencent_id;
            $package_id    = $log->package_id;
            $iegame        = $log->iegame;

            $status = "success";

            if ($log->status == "pending"){

                $this->info(" Sending $package_id ( $iegame ) to $tencent_id ");

                $response = $this->aovSendItem($tencent_id,$package_id,$iegame,"test_rescue_alice");
                $arrResponse = json_decode($response);

                if (isset($arrResponse->ItemList) === false) {
                    $status   = "fail";
                } elseif ($arrResponse->ItemList == []) {
                    $status   = 'fail';
                } elseif (isset($arrResponse->msg)) {
                    $status   = 'fail';
                } else {
                    $success  += 1;
                }

                $log->update([
                    'status'    => $status,
                    'response'  => $response,
                ]);

                $total++;
            } else{
                $this->info(" Pass not pending ".$log->uid);
            }
        }

        $this->info(" ---------------------------");
        $this->info(" Total \t\t$total");
        $this->info(" Success \t$success");
        $this->info(" Goodbye.");
    }

    public function aovSendItem(string $tencentID,int $packageID,string $iegamskey,string $eventName){
        $iegamssep = explode( '-',$iegamskey );
        $AMSSerial = "AMS-".$eventName."-".Carbon::now()->format("mdhis")."-".$this->generateRandomString(6)."-".$iegamssep[1]."-".$iegamssep[2];
        $ams = [];
        $ams['AmsInfo']         = $iegamskey;
        $ams['AmsSerial']       = $AMSSerial;
        $ams['EventName']       = $eventName;
        $ams['Language']        = "th";
        $ams['PackageGroupID']  = $packageID;

        $result = $this->gameProxyGarenanowCurlPostJSONRaw(
            "https://th.game.proxy.garenanow.com/game/aov/tidy/v1/senditem/".$tencentID,
            $ams
        );
        return $result;
    }

    public function gameProxyGarenanowCurlPostJSONRaw($url, $reqBody){
        $curl = curl_init();
        $ClientId = "f1099ef1-c2db-4926-bfee-fe4af126af2f";
        $myJSON = json_encode($reqBody);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $myJSON,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                // "Client-ID:".$client_id
                "Client-ID:".$ClientId
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}