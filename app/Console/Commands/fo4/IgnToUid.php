<?php

namespace App\Console\Commands\fo4;

use Illuminate\Console\Command;

class IgnToUid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fo4:ign_to_uid';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list_ign = [
        'ESIES061',
        'ESIES062',
        'ESIES063',
        'ESIES064',
        'ESIES065',
        'ESIES066',
        'ESIES067',
        'ESIES068',
        'ESIES069',
        'ESIES070'];
        foreach ($list_ign as $ign) {
            $accountSearch = $this->account_search($ign);

            if($accountSearch->data->result == false){
                return $accountSearch->data->reason ;
            }

            $this->info($accountSearch->data->owner);
        }
    }



    public function account_search($ownername){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.apps.garena.in.th",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "key_name=fo4&service=account_search&env=live&ownername=".$ownername,
            CURLOPT_HTTPHEADER => array(
              "Content-Type: application/x-www-form-urlencoded"
            ),
          ));

        $response = curl_exec($curl);

        curl_close($curl);
        $myJson = json_decode($response);

        return $myJson ;
    }
}


