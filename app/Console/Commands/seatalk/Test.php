<?php

namespace App\Console\Commands\seatalk;

use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seatalk:test';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('What is your name ?');
        if (empty($name) || $name == "" || $name === null) {
            $this->info(" Pls, enter your name!, Goodbye.");
            return;
        }

        $this->sendSeeTalk("https://openapi.seatalk.io/webhook/group/MQN_UN9vSwyj0WM2i6PNwg",$name,true,[]);
    }



    public function sendSeeTalk($url, $text,$tagall,$tagMail){
        $curl = curl_init();

        $myObj = [];
        $myObj['tag'] = "text";
        $myObj['text']['content'] = $text;
        if($tagall === true){
            $myObj['text']['at_all'] = $tagall;
        } elseif(isset($tagMail)){
            $myObj['text']['mentioned_email_list'] = $tagMail;
        }
        
        $myJSON = json_encode($myObj);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $myJSON,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        $this->info($response);
        curl_close($curl);
    }
}
