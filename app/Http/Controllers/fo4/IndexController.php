<?php

namespace App\Http\Controllers\fo4;

use App\Models\fo4\Events;
use App\Models\fo4\SendLogs;
use App\Models\fo4\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;

class IndexController extends Controller
{
    public function index()
    {
        $events     = Events::orderBy('_id', 'desc')->get();
        $user_count = $this->getUsersFiveEvent($events);

        $data = [
            'events'     => $events,
            'user_count' => $user_count,
        ];

        return view('fo4.index', $data);
    }

    public function add()
    {
        $data = [];
        return view('fo4.add', $data);
    }

    public function addStore(Request $request)
    {
        $last = Events::orderBy('_id', 'desc')->first();
        if (!$last) {
            $event_id = 1;
        } else {
            $event_id = (int) $last->event_id + 1;
        }

        $event                     = new Events();
        $event->event_id           = $event_id;
        $event->event_name         = $request->event_name;
        $event->event_slug         = $request->event_slug;
        $event->event_start        = (string) Carbon::parse($request->event_start)->toDateTimeString();
        $event->event_end          = (string) Carbon::parse($request->event_end)->toDateTimeString();
        $event->api_end            = (string) Carbon::parse($request->api_end)->toDateTimeString();
        $event->client_id          = $request->client_id ?? null;
        $event->maintenance        = true;
        $event->global_maintenance = true;
        $event->primary            = true;
        $event->sending_item       = true;
        $event->deducts_fcmc       = true;
        $event->default_group      = $request->default_group;
        $event->link_video         = $request->link_video;
        $event->add_milestone      = 0;

        $event->save();

        Alert::success('Successfully', sprintf("Add new event as %s", $event->event_name));
        return redirect()->route('fo4.index');
    }

    public function senditem()
    {
        $events   = Events::orderBy('_id', 'desc')->select('event_name', 'event_slug')->get();
        $sendlogs = SendLogs::orderBy('_id', 'desc')->paginate(30);
        $data     = [
            'events'   => $events,
            'sendlogs' => $sendlogs,
        ];

        return view('fo4.senditem', $data);
    }

    public function senditemPost(Request $request)
    {
        $account_id = $request->account_id;
        $product_id = (int) $request->product_id;
        $event      = $request->event;
        $purpose    = $request->purpose;
        $count      = (int) $request->count;

        $sendlogs                 = new SendLogs();
        $sendlogs->account_id     = $account_id;
        $sendlogs->product_id     = $product_id;
        $sendlogs->event          = $event;
        $sendlogs->purpose        = $purpose;
        $sendlogs->count          = $count;
        $sendlogs->status         = 'pending';
        $sendlogs->created_at_str = Carbon::now()->toDateTimeString();
        $sendlogs->ip             = GameAPI::getIP();
        $sendlogs->send_by        = "myreport";
        $sendlogs->save();

        $response = GameApi::inboxAdd($sendlogs->account_id, $sendlogs->product_id, $sendlogs->count);

        if ($response && $response->status && $response->data && $response->data->result) {
            $sendlogs->status = 'success';
        } else {
            $sendlogs->status = 'fail';
        }
        $sendlogs->response = $response;
        $sendlogs->save();

        Alert::success('Successfully', 'Send item');
        return redirect()->route('fo4.senditem', [
            'product_id' => $product_id,
            'event'      => $event,
        ]);
    }

    private function getUsersFiveEvent($events)
    {
        $events      = $events->take(10);
        $result_name = $result_count = [];

        foreach ($events as $event) {
            $dbname = 'fo4_'.$event->event_slug."_users";
            $db     = new User();
            $db->setTable($dbname);
            $user           = $db->count();
            $result_name[]  = $event->event_name;
            $result_count[] = $user;
        }
        return [
            'name'  => $result_name,
            'count' => $result_count,
        ];
    }

    private function getSendLogs($event)
    {
        $dbname = 'fo4_'.$event->event_slug."_send_logs";
        $db     = new User();
        $db->setTable($dbname);
        $result_date = $result_success = $result_resend = [];
        $ranges      = $this->dateRange($event);
        foreach ($ranges as $date) {
            $success          = $db->whereStatus('success')->where('created_at', '<',
                Carbon::parse($date)->endOfDay())->count();
            $resend           = $db->where('resend_at_str', 'is not null')->where('created_at', '<',
                Carbon::parse($date)->endOfDay())->count();
            $result_date[]    = $date;
            $result_success[] = $success;
            $result_resend[]  = $resend;
        }

        return [
            'date'    => $result_date,
            'success' => $result_success,
            'resend'  => $result_resend,
        ];
    }

    private function dateRange($event)
    {
        $start = Carbon::parse($event->event_start);
        $end   = Carbon::parse($event->event_end);
        $today = Carbon::today();
        $dates = [];

        for ($date = $start->copy(); $date->lte($end); $date->addDay()) {
            if ($today < $date) {
                continue;
            }
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

    public function eventUpdateStatus(Request $request)
    {
        $event = Events::where('event_slug', $request->slug)->first();
        if (!$event) {
            return response()->json(['status' => false, 'message' => 'Not Found event '.$request->slug]);
        }

        $type = $request->type;
        if ($type == 1) {
            $type_name = "global_maintenance";
        } elseif ($type == 2) {
            $type_name = "maintenance";
        } elseif ($type == 3) {
            $type_name = "primary";
        } elseif ($type == 4) {
            $type_name = "deducts_fcmc";
        } elseif ($type == 5) {
            $type_name = "sending_item";
        }

        $status = $event->{$type_name};
        if ($status === false) {
            $status_name         = 'online';
            $event->{$type_name} = true;
        } else {
            $status_name         = 'maintenance';
            $event->{$type_name} = false;

        }
        $event->save();
        return response()->json([
            'status'  => true,
            'message' => $status_name,
        ]);
    }

    private function getConnectionConfig()
    {
        $value = env('DB_FO4_EVENT_MONGODB_DATABASE');
        return $value;
    }

    private function getLOLConnectionConfig()
    {
        $value = env('DB_LOL_EVENT_MONGODB_DATABASE');
        return $value;
    }

    public function changeEnv()
    {
        $this->setConnectionConfig();
        return back();
    }

    private function setConnectionConfig()
    {
        if ($this->getConnectionConfig() === "events_fo4") {
            $this->setEnvironmentValue('DB_FO4_EVENT_MONGODB_DATABASE', 'events_fo4_test');
        } else {
            $this->setEnvironmentValue('DB_FO4_EVENT_MONGODB_DATABASE', 'events_fo4');
        }


        if ($this->getLOLConnectionConfig() === "events_lol") {
            $this->setEnvironmentValue('DB_LOL_EVENT_MONGODB_DATABASE', 'events_lol_test');
        } else {
            $this->setEnvironmentValue('DB_LOL_EVENT_MONGODB_DATABASE', 'events_lol');
        }
    }

    public function setEnvironmentValue($envKey, $envValue)
    {
        $envFile = app()->environmentFilePath();
        $str     = file_get_contents($envFile);

        $str               .= "\n"; // In case the searched variable is in the last line without \n
        $keyPosition       = strpos($str, "{$envKey}=");
        $endOfLinePosition = strpos($str, PHP_EOL, $keyPosition);
        $oldLine           = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);
        $str               = str_replace($oldLine, "{$envKey}={$envValue}", $str);
        $str               = substr($str, 0, -1);

        $fp = fopen($envFile, 'w');
        fwrite($fp, $str);
        fclose($fp);
    }

    public function eventEdit(Events $event)
    {
        $data = [
            'event' => $event,
        ];
        return view('fo4.edit', $data);
    }

    public function eventEditPost(Events $event, Request $request)
    {
        $event->event_name  = $request->event_name;
        $event->event_slug  = $request->event_slug;
        $event->event_start = (string) Carbon::parse($request->event_start)->toDateTimeString();
        $event->event_end   = (string) Carbon::parse($request->event_end)->toDateTimeString();
        $event->api_end     = (string) Carbon::parse($request->api_end)->toDateTimeString();
        $event->client_id   = $request->client_id ?? null;
        $event->link_video   = $request->link_video ?? null;

        try {
            $event->save();
            Alert::success('Successfully', 'Save');
        } catch (\Exception $x) {
            Alert::error('Error', $x->getMessage());
            return back();
        }

        return redirect(route('fo4.edit', [$event]));
    }

    public function dashboard(Events $event)
    {
        $daily    = $this->getDailyUser($event);
        $sendlogs = $this->getSendlogsEvent($event);
        $data     = [
            'event'    => $event,
            'daily'    => $daily,
            'sendlogs' => $sendlogs,
        ];
        return view('fo4.dashboard', $data);
    }

    private function getDailyUser(Events $event)
    {
        $dbname     = 'fo4_'.$event->event_slug."_daily";
        $start_date = Carbon::parse($event->event_start);
        $end_date   = Carbon::parse($event->event_end);
        $dates      = [];

        for ($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        $return      = [];
        $return_date = [];
        foreach ($dates as $date) {
            if (Carbon::today()->toDateString() < $date) {
                continue;
            }
            $db = new User();
            $db->setTable($dbname);
            $count         = $db->where('date', $date)->count() ?? 0;
            $return_date[] = $date;
            $return[]      = $count;
        }

        return [
            'name'  => $return_date,
            'count' => $return,
        ];
    }

    private function getSendlogsEvent(Events $event)
    {
        $dbname = 'fo4_'.$event->event_slug."_send_logs";
        $arr    = ['success', 'inqueue', 'fail'];
        $return = [];

        foreach ($arr as $item) {
            $db = new User();
            $db->setTable($dbname);
            $count    = $db->where('status', $item)->count() ?? 0;
            $return[] = $count;
        }

        return $return;
    }
}
