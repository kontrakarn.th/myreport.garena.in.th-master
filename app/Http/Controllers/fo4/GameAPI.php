<?php
namespace App\Http\Controllers\fo4;

use GuzzleHttp\Client;

class GameAPI
{
    private const KEY_NAME = 'fo4';
    private const API_URL = 'http://api.apps.garena.in.th';

    public static function getIP()
    {

        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {

            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            return array_shift($ips);

        } else {

            return request()->getClientIp();

        }

    }
    /**
     * send product to fo4 user inbox
     *
     * @param string    $accountId  FO4 user ID
     * @param int       $productId  Product ID
     * @param int       $count      Product count
     * @param string    $env        env
     * @return array|boolean
     */
    public static function inboxAdd(string $accountId, int $productId, int $count = 1, string $env = 'live')
    {
        if (empty($accountId) || empty($productId)) {
            return false;
        }

        $client = new Client();
        $response = $client->request('POST', self::API_URL, [
            'form_params' => [
                'key_name'      => self::KEY_NAME,
                'service'       => 'inbox_add',
                'accountid'     => $accountId,
                'product_id'    => $productId,
                'count'         => $count,
                'env'           => $env
            ]
        ]);

        if ($response->getStatusCode() !== 200) {
            return false;
        }

        return json_decode((string) $response->getBody());
    }
}
