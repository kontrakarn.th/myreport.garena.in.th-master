<?php

namespace App\Http\Controllers\lol;

use App\Models\lol\AutoPopup;
use App\Models\lol\SendLogs;
use App\Models\lol\VisitorHub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{

    public function senditem()
    {
        $sendlogs = SendLogs::orderBy('id', 'desc')->paginate(30);
        $data     = [
            'sendlogs' => $sendlogs,
        ];

        return view('lol.senditem', $data);
    }

    public function visitorHub()
    {
        $visitors = VisitorHub::orderBy('id', 'desc')->paginate(30);
        $data     = [
            'visitors' => $visitors,
        ];

        return view('lol.visitor-hub', $data);
    }

    public function autoPopup()
    {
        $popup = AutoPopup::first();
        $data  = [
            'popup' => $popup,
        ];

        return view('lol.auto-popup', $data);
    }

    public function autoPopupSave(Request $request)
    {
        $popup = AutoPopup::first();
        if ($popup) {
            $popup->url = $request->url;
            $popup->save();
        }

        return redirect()->back();
    }

    public function autoPopupStatus(Request $request)
    {
        $popup = AutoPopup::first();
        if ($popup) {

            if ($request->type === "live") {
                if ($popup->status_live === "active") {
                    $popup->status_live = "inactive";
                } else {
                    $popup->status_live = "active";
                }
            }

            if ($request->type === "test") {
                if ($popup->status_test === "active") {
                    $popup->status_test = "inactive";
                } else {
                    $popup->status_test = "active";
                }
            }

            $popup->save();
            $this->clearCache();

            return [
                'status'  => true,
                'message' => '',
            ];

        }

        return [
            'status'  => false,
            'message' => '',
        ];
    }

    public function clearCache()
    {
        Cache::forget('lolapi:auto-popup');
        return true;
    }

    public function autoPopupClearCache()
    {
        $this->clearCache();
        return redirect()->back();
    }

}
